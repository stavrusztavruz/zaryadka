window.onload = function () {

    const windowOuterWidth = window.outerWidth
    console.log(windowOuterWidth)
    if (windowOuterWidth >= 600) {
        $('.slider1').slick(
            {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                speed: 700,
                autoplay: true,
                autoplaySpeed: 4000
            }
        );
        $('.slider2').slick(
            {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                speed: 700,
                autoplay: true,
                autoplaySpeed: 4000
            }
        );
        $('.slider3').slick(
            {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                speed: 700,
                autoplay: true,
                autoplaySpeed: 4000
            }
        );
        $('.slider4').slick(
            {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: false,
                speed: 700,
                autoplay: true,
                autoplaySpeed: 4000
            }
        );

        let boxImages_img1 = document.querySelectorAll('.slider1_img1');
        let boxImages_img2 = document.querySelectorAll('.slider1_img2');
        boxImages_img1.forEach((item) => {
            item.style.display = "flex"
        })
        boxImages_img2.forEach((item) => {
            item.style.display = "none"
        })
    } else {
        $('.slider1').slick(
            {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                speed: 700,
                autoplay: true,
                autoplaySpeed: 4000
            }
        );
        $('.slider2').slick(
            {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                speed: 700,
                autoplay: true,
                autoplaySpeed: 4000
            }
        );
        $('.slider3').slick(
            {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                speed: 700,
                autoplay: true,
                autoplaySpeed: 4000
            }
        );
        $('.slider4').slick(
            {
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                speed: 700,
                autoplay: true,
                autoplaySpeed: 4000
            }
        );


        let boxImages_img1 = document.querySelectorAll('.slider1_img1');
        let boxImages_img2 = document.querySelectorAll('.slider1_img2');
        boxImages_img1.forEach((item) => {
            item.style.display = "none"
        })
        boxImages_img2.forEach((item) => {
            item.style.display = "flex"
        })

    }


    let headerMini = document.querySelector('.headerMini');
    let menuMini = document.querySelector('.menuMini');
    let btnOpen = document.querySelector('.btnOpen');
    let btnClose = document.querySelector('.btnClose');
    btnOpen.addEventListener('click', () => {
        menuMini.style.display = 'flex';
        headerMini.style.display = 'none';
    });
    btnClose.addEventListener('click', () => {
        menuMini.style.display = 'none';
        headerMini.style.display = 'flex';
    })


}